package com.java.main;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;


public class GuessNumber {

    private static final String MSG_WELCOME = "Добро пожаловать в игру - \"Угадай число!\"";
    private static final String MSG_ARTICLE = "____________________________________________";
    private static final String MSG_COMMAND_LIST = "Для помощи введи - help \n Для начала игры введи любое число";

    private static final String COMMAND_HELP = "help";
    private static final String COMMAND_HELP_LIST = "Для установки диапазона введите \"range\" \n" +
            "Для установки количества попыток введите \"count\" \n" +
            "Для выхода из программы введите \"exit\" \n" +
            "Для начала игры введите положительно целое число";
    private static final String COMMAND_SET_RANGE = "range";
    private static final String COMMAND_COUNT = "count";
    private static final String COMMAND_EXIT = "exit";


    public void startGame(Scanner scanner) {
        startGame(scanner, null);

    }

    private void startGame(Scanner scanner, Guesser guesser) {
        if (guesser == null) {
            System.out.println(MSG_WELCOME + "\n" + MSG_ARTICLE);
             guesser = new Guesser();
            System.out.println(guesser.startInfo());
        }else {
            System.out.println(guesser.startInfo());
            System.out.println("Введите число");
        }
        if(scanner.hasNextInt()){
            runGame(guesser, scanner);
        } else {
checkCommand(scanner, scanner.next());
        }
    }
    private void checkCommand (Scanner scanner, String command) {
        switch (command.toLowerCase()) {
            case COMMAND_SET_RANGE :{
                createNewGuessedByRange(scanner);
                break;
            }
            case COMMAND_COUNT :{
                int count = setCount(scanner);
                Guesser guesser = new Guesser();
                guesser.setCount(count);
                startGame(scanner, guesser);
                break;
            }
            case COMMAND_EXIT :{
                return;
            }
            case COMMAND_HELP :{
                System.out.println(COMMAND_HELP_LIST);
                startGame(scanner);
            }

            default:{
                System.out.println("Не правильный ввод команды");
                startGame(scanner);
            }
        }
    }

    private void  runGame (Guesser guesser, Scanner scanner){
        int inputNumber = scanner.nextInt();
        while (guesser.checkGuessNumber(inputNumber) && guesser.checkCount()){
            if(!scanner.hasNextInt() && isExit(scanner.next())){
                return;
            }
            System.out.println("Ведите число ");
            inputNumber = scanner.nextInt();
        }
    }
    private boolean isExit(String command){
        return command.trim().equalsIgnoreCase("exit");
    }
    private void createNewGuessedByRange (Scanner scanner) {
        int minNumber = 0;
        int maxNumber = 0;
        do {
            System.out.println("Введи позитивные целые числа не больше 200!");

            while (!scanner.hasNextInt()) {
                System.out.println("Это не целое число! Попробуй ввести ещё раз!");
                scanner.next();
            }
            minNumber = scanner.nextInt();
            try {
                maxNumber = scanner.nextInt();
            } catch (InputMismatchException e) {
                scanner.next();
            }

        } while (minNumber <= 1 || maxNumber <= 1 || maxNumber > 200 || minNumber >= maxNumber);


        if (minNumber > maxNumber) {
            int tmp = maxNumber;
            maxNumber = minNumber;
            minNumber = tmp;
        }
        Guesser guesser = new Guesser(minNumber, maxNumber);
        startGame(scanner, guesser);
    }

    public int setCount (Scanner scanner) {
        System.out.println("Введите целое положительно число от 1 до 15");
        if(!scanner.hasNextInt()) {
            scanner.next();
        }
        int count;
        do {
            System.out.println("Неверный ввод! Введи число от 1 до 15");
            while (!scanner.hasNextInt()) {
                System.out.println("Это не число! Введи число от 1 до 15");
                scanner.next();
            }
            count = scanner.nextInt();
        } while (count <= 0 || count > 15);
        System.out.println("Теперь количество попыток равно " + count);

        return count;

    }


}

