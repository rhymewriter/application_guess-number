package com.java.main;

import java.util.Random;

public class Guesser {
    private Random random = new Random();
    private static final String MSG_WIN = "Ты угадал!";
    private static final String MSG_COLD = "Не угадал, холодно!";
    private static final String MSG_WARMER = "Не угадал, тепло!";

    private int count;
    private int minRange;
    private int maxRange;
    private int randomNumber;
    private int guessNumber;

    public Guesser(int minRange, int maxRange) {
        this.minRange = minRange;
        this.maxRange = maxRange;
        this.count = 5;
        getRandomNumber();
    }

    public Guesser() {
        this.count = 20;
        this.minRange = 1;
        this.maxRange = 100;
        getRandomNumber();
    }

    public Guesser(int count, int minRange, int maxRange) {
        this.count = count;
        this.minRange = minRange;
        this.maxRange = maxRange;
        getRandomNumber();
    }

    public boolean checkCount () {
        if(count > 0) {
            System.out.println("Осталось попыток: " + count);
            count--;
            return true;
        } else {
            System.out.println("Количество попыток исчерпано!!");
            return false;
        }

    }

    public boolean checkGuessNumber (int number) {
        if(number == randomNumber) {
            System.out.println(MSG_WIN);
            return false;
        }
        if(guessNumber == 0) {
            System.out.println("Не угадал!");
            guessNumber = number;
            return true;
        }
        int firstGuess = guessNumber > randomNumber ? guessNumber - randomNumber : randomNumber - guessNumber;
        int secondGuess = number > randomNumber ? number - randomNumber : randomNumber - number;
        System.out.println(firstGuess < secondGuess ? MSG_COLD : MSG_WARMER);
        guessNumber = number;
        return true;
    }

    public String startInfo() {
        return "Я загадал число от " + minRange + " до " + maxRange + " вашего диапазона. Попробуй угадать" +
                " его за " + count + " попыток";

    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getMinRange() {
        return minRange;
    }

    public void setMinRange(int minRange) {
        this.minRange = minRange;
    }

    public int getMaxRange() {
        return maxRange;
    }

    public void setMaxRange(int maxRange) {
        this.maxRange = maxRange;
    }

    public int getRandomNumber() {
        randomNumber = random.nextInt(maxRange - minRange) + minRange;
        return randomNumber;
    }
}
