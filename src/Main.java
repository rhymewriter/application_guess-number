import com.java.main.GuessNumber;
import com.java.main.Guesser;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        new GuessNumber().startGame(new Scanner(System.in));

    }
}
